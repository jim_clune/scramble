-- Scramble game dynamics: the [update] function and supporting functions.

module Update exposing (..)
import Time exposing (Time)
import Model exposing (..)
import Vehicle exposing (Vehicle)
import Obstacle exposing (Obstacle)
import Collision exposing (convexObjectsCollide)
import List.Extra
import Random
import Math.Vector2 exposing (..)

bound : Float -> Float -> Float -> Float -> Float
bound unbounded headRoom tailRoom size =
  Basics.max tailRoom (Basics.min (size - headRoom) unbounded)

-- Update vertical position and velocity of of object based on gravity,
-- size constraints, and velocity.
verticalPosAndVelocity : Vec2 -> Float -> Float -> Float -> (Vec2, Float)
verticalPosAndVelocity pos velocity headRoom tailRoom =
  let newY = bound (getY pos + velocity) headRoom tailRoom totalHeight in
  let v = velocity + gravity in
  let newVelocity =
    if newY <= tailRoom && v <= 0 then
      0
    else
      if newY >= totalHeight - headRoom && v > 0 then
        0
      else
        v
  in
  (setY newY pos, newVelocity)

-- Update vertical position and velocity of model.
updateVertical : Model -> Model
updateVertical model =
  let headRoom = Vehicle.topToDriver model.vehicle in
  let tailRoom = Vehicle.bottomToDriver model.vehicle in
  let (pos, verticalVelocity) =
    verticalPosAndVelocity model.pos model.verticalVelocity headRoom tailRoom
  in
  { model | pos = pos, verticalVelocity = verticalVelocity }

horizontalDelta : Model -> Float
horizontalDelta model =
  let enabled =
    case model.vehicle of
      Vehicle.Drill ->
        model.verticalVelocity == 0
          && getY model.pos <= Vehicle.bottomToDriver model.vehicle
      _ -> True
  in
  let
    absDelta = 15
  in
  if enabled then
    case (model.left, model.right) of
      (True, False) -> -absDelta
      (False, True) -> absDelta
      (False, False) -> 0
      (True, True) ->
        case model.mostRecent of
          Left -> -absDelta
          Right -> absDelta
  else
    0

-- Update horizontal position based on state of left/right keys.
updateHorizontal : Model -> Model
updateHorizontal model =
  let deltaX = getX model.pos + horizontalDelta model in
  let headRoom = Vehicle.frontToDriver model.vehicle in
  let tailRoom = Vehicle.backToDriver model.vehicle in
  let x = bound deltaX headRoom tailRoom totalWidth in
  { model | pos = vec2 x (getY model.pos)}

-- Apply any vehicle-specific updates. Assumes the normal position
-- and velocity update has already been applied.
vehicleSpecificUpdate : Model -> Model
vehicleSpecificUpdate model =
  case model.vehicle of
    Vehicle.Car -> model
    Vehicle.Drill ->
      let verticalVelocity =
        if model.up then
          15 - gravity
        else
          model.verticalVelocity
      in
      { model | verticalVelocity = verticalVelocity }
    Vehicle.Skateboard ->
      let verticalVelocity =
        if model.up && getY model.pos <= Vehicle.bottomToDriver model.vehicle then
          25
        else
          model.verticalVelocity
      in
      { model | verticalVelocity = verticalVelocity }
    Vehicle.Motorcycle ->
      if model.up && model.bullet == Nothing then
        let bullet =
          { pos = model.pos, verticalVelocity = 35 }
        in
        { model | bullet = Just bullet }
      else
        model
    Vehicle.Helicopter ->
      let verticalVelocity =
        if model.up then
          model.verticalVelocity - 2 * gravity
        else
          model.verticalVelocity
      in
      { model | verticalVelocity = verticalVelocity }
    Vehicle.Pogo ->
      let verticalVelocity =
        if getY model.pos <= Vehicle.bottomToDriver model.vehicle then
          if model.up then
            30
          else
            15
        else
          model.verticalVelocity
      in
      { model | verticalVelocity = verticalVelocity }
    Vehicle.Rocket ->
      let verticalVelocity =
        if model.up then
          18 - gravity
        else
          model.verticalVelocity
      in
      { model | verticalVelocity = verticalVelocity }

bulletVertices : Bullet -> List Vec2
bulletVertices bullet =
  let (x, y) = toTuple bullet.pos in
  let radius = 10 in
  [ vec2 (x-radius) (y+radius)
  , vec2 (x+radius) (y+radius)
  , vec2 (x+radius) (y-radius)
  , vec2 (x-radius) (y-radius)
  , vec2 (x-radius) (y+radius) ]

vehicleVertices : Model -> List Vec2
vehicleVertices model =
  -- Translate from position relative to driver to position relative to upper left.
  let xOffset = -(Vehicle.backToDriver model.vehicle) in
  let yOffset = Vehicle.topToDriver model.vehicle in
  let pos = add model.pos (vec2 xOffset yOffset) in
  Vehicle.vertices model.vehicle
  |> List.map (add pos)

-- The effective vertical position of the boxing glove depends on how far the glove
-- is extended, so we adjust the effectiveY accordingly.
boxingGloveEffectiveDeltaY : Int -> Float
boxingGloveEffectiveDeltaY ticks =
  -0.1 * (toFloat (abs (((ticks // 3) % 12) - 6)))
      * Obstacle.height Obstacle.BoxingGlove

obstacleVertices : ObstacleState -> Int -> List Vec2
obstacleVertices obsState ticks =
  let pos =
    if obsState.obstacle == Obstacle.BoxingGlove then
      setY (getY obsState.pos + boxingGloveEffectiveDeltaY ticks) obsState.pos
    else
      obsState.pos
  in
  Obstacle.vertices obsState.obstacle
  |> List.map (add pos)

bulletHitsObstacle : Maybe Bullet -> ObstacleState -> Int -> Bool
bulletHitsObstacle maybeBullet obsState ticks =
  let obs = obsState.obstacle in
  if obs == Obstacle.Card then
    False
  else
    case maybeBullet of
      Nothing -> False
      Just bullet ->
        convexObjectsCollide (bulletVertices bullet) (obstacleVertices obsState ticks)

bulletHitsVehicle : Model -> Bool
bulletHitsVehicle model =
  case model.bullet of
    Nothing -> False
    Just bullet ->
      -- Hack: we don't want the bullet to hit the vehicle while the vehicle is
      -- shooting it, so we don't consider it a hit on the vehicle while the
      -- bullet has just been shot upwards.
      bullet.verticalVelocity <= 32
      && convexObjectsCollide (bulletVertices bullet) (vehicleVertices model)

nextObstacleState : Model -> ObstacleState -> Maybe ObstacleState
nextObstacleState model obsState =
  let pos = obsState.pos in
  let obs = obsState.obstacle in
  if getX pos < -(Obstacle.width obs) || getY pos < -(Obstacle.height obs)
    || bulletHitsObstacle model.bullet obsState model.ticks then
    Nothing
  else
    let x = getX pos - 10 in
    let (y, verticalVelocity, extra) =
      case obs of
        Obstacle.Stalactite ->
          if getX pos <= model.dropStalactiteAt then
            (getY pos + obsState.verticalVelocity
            , obsState.verticalVelocity + gravity, 0)
          else
            (getY pos, 0, obsState.extra)
        Obstacle.SpikeBall -> (getY pos, 0, obsState.extra)
        Obstacle.SpikeFloor -> (getY pos, 0, obsState.extra)
        Obstacle.BounceBall ->
          let candidateY = getY pos + obsState.verticalVelocity in
          if candidateY <= Obstacle.height obs
            && obsState.verticalVelocity < 0
            && obsState.extra == 0
          then
            -- Time to flex!
            (Obstacle.height obs, -obsState.verticalVelocity, 80)
          else if obsState.extra > 0 then
            (getY pos, obsState.verticalVelocity, obsState.extra - 4)
          else if candidateY <= Obstacle.height obs
            && obsState.verticalVelocity > 0
            && obsState.extra == 0 then
            -- Time to bounce!
            (Obstacle.height obs + obsState.verticalVelocity
            , -obsState.verticalVelocity, 0)
          else
            (candidateY, obsState.verticalVelocity + gravity, 0)
        Obstacle.BoxingGlove -> (getY pos, 0, obsState.extra)
        Obstacle.Card -> (getY pos - 8, 0, obsState.extra)
        Obstacle.SwapBox -> (getY pos, 0, obsState.extra)
    in
    Just { obstacle = obs
         , pos = vec2 x y
         , verticalVelocity = verticalVelocity
         , extra = extra }

hitObstacle : Model -> Maybe Obstacle
hitObstacle model =
  List.Extra.find
    (\obsState ->
       let obs = obsState.obstacle in
       if obs == Obstacle.Card then
         -- Cards only act to impair vision.
         False
       else
         convexObjectsCollide (obstacleVertices obsState model.ticks) (vehicleVertices model))
    model.obstacles
  |> Maybe.map (\obsState -> obsState.obstacle)

needNewBuilding : Model -> Bool
needNewBuilding model =
  case model.buildings of
    [] -> True
    first::_ ->
      first.absX + buildingWidth < model.ticks * 2

maxDifficulty = 4

onTime : Time -> Model -> (Model, Cmd Msg)
onTime time model =
  let touchingObstacle = hitObstacle model in
  if touchingObstacle == Just Obstacle.SwapBox
    && time - model.lastChanged > 1000
    && model.remainingTransitions == 0 then
    ({ model | lastChanged = time, remainingTransitions = 4},
       Random.generate SetVehicle (Random.int 0 6))
  else if time - model.lastChanged > 500 && model.remainingTransitions > 0 then
    ({ model | lastChanged = time,
      remainingTransitions = model.remainingTransitions - 1},
      Random.generate SetVehicle (Random.int 0 7))
  else if model.remainingTransitions > 0 then
    (model, Cmd.none)
  else if ((Maybe.map Obstacle.causesGameOver touchingObstacle
           |> Maybe.withDefault False)
          || bulletHitsVehicle model)
       then
    ({ model | gameOver = True }, Cmd.none)
  else
    let newModel =
      updateHorizontal model
      |> updateVertical
      |> vehicleSpecificUpdate
    in
    let obsts =
      List.filterMap (nextObstacleState newModel) newModel.obstacles
    in
    if needNewBuilding model then
      let cmd = Random.generate SpawnBuilding (Random.list 7 (Random.int 1 6)) in
      ({ newModel | ticks = newModel.ticks + 1
       , obstacles = obsts }, cmd)
    else if time - model.lastObstacleTime > model.timeBetweenObstacles
      && model.buildingNumber > 35
      && (List.map (\s -> Obstacle.difficulty s.obstacle) model.obstacles |> List.sum) < maxDifficulty then
      let cmd =
        Random.generate SpawnObstacle (Random.pair (Random.int 0 100) (Random.float 0 1))
      in
      ({ newModel | ticks = newModel.ticks + 1
       , lastObstacleTime = time
       , timeBetweenObstacles = model.timeBetweenObstacles * 0.97
       , obstacles = obsts }, cmd)
    else
      ({ newModel | ticks = newModel.ticks + 1
       , obstacles = obsts }, Cmd.none)

onPress : Char -> Model -> Model
onPress char model =
    case char of
        'A' -> { model | left = True, mostRecent = Left }
        'D' -> { model | right = True, mostRecent = Right}
        'W' -> { model | up = True }
        _ -> model

onRelease : Char -> Model -> Model
onRelease char model =
    case char of
        'A' -> { model | left = False }
        'D' -> { model | right = False }
        'W' -> { model | up = False }
        _ -> model

initialObsY : Obstacle -> Float -> Float
initialObsY obs paramNum =
  case obs of
    Obstacle.BounceBall -> totalHeight * (1 + paramNum) / 2
    Obstacle.BoxingGlove -> Obstacle.height obs
    Obstacle.SpikeBall ->
      Obstacle.height obs + (totalHeight - Obstacle.height obs) * paramNum
    Obstacle.SpikeFloor -> 0
    Obstacle.Card -> totalHeight + (Obstacle.height obs) * (1 + paramNum/2)
    Obstacle.Stalactite -> totalHeight
    Obstacle.SwapBox -> Obstacle.height obs

update : Msg -> Model -> (Model, Cmd Msg)
update msg oldModel =
  if oldModel.gameOver then
    (oldModel, Cmd.none)
  else
    let bullet =
      case oldModel.bullet of
        Just bullet ->
          let newY = getY bullet.pos + bullet.verticalVelocity in
          if newY < 0 then
            Nothing
          else
            let (pos, verticalVelocity) =
              verticalPosAndVelocity bullet.pos bullet.verticalVelocity 0 0
            in
              Just { pos = pos, verticalVelocity = verticalVelocity }
        Nothing -> Nothing
    in
      let model = { oldModel | bullet = bullet } in
      case msg of
          Tick time -> onTime time model
          Press char -> (onPress char model, Cmd.none)
          Release char -> (onRelease char model, Cmd.none)
          SetVehicle n ->
            ({ model | vehicle = Vehicle.ofInt n, verticalVelocity = 0 }, Cmd.none)
          SpawnBuilding values ->
            let oldBuildings =
              if List.length model.buildings < 30 then
                model.buildings
              else
                case List.tail model.buildings of
                  Just b -> b
                  Nothing -> []
            in
            let newBuilding = generateBuilding model.buildingNumber values in
            let buildingNumber = model.buildingNumber + 1 in
            ({ model | buildings = oldBuildings ++ [newBuilding]
             , buildingNumber = buildingNumber }, Cmd.none)
          SpawnObstacle (obsNum, paramNum) ->
            let possibleObstacles =
               Obstacle.compatibleWith model.vehicle
               |> List.drop 1
            in
            case List.Extra.getAt (obsNum % (List.length possibleObstacles))
              possibleObstacles of
              Nothing ->
                -- This should be unreachable.
                (model, Cmd.none)
              Just obs ->
                let x = totalWidth + Obstacle.width obs in
                let newObs =
                  if obs == Obstacle.SpikeFloor then
                    [ { obstacle = obs
                      , pos = vec2 x totalHeight
                      , verticalVelocity = 0
                      , extra = 0 }
                    , { obstacle = obs
                      , pos = vec2 x (Obstacle.height obs)
                      , verticalVelocity = 0
                      , extra = 0 } ]
                  else
                    let obsPos =
                      vec2 (totalWidth + Obstacle.width obs) (initialObsY obs paramNum)
                    in
                    let extra =
                      if obs == Obstacle.Card then
                        model.ticks % 2
                      else
                        0
                    in
                    [ { obstacle = obs
                      , pos = obsPos
                      , verticalVelocity = 0
                      , extra = extra } ]
                in
                ({ model | dropStalactiteAt = paramNum * totalWidth
                  , obstacles = newObs ++ model.obstacles}, Cmd.none)
