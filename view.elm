-- Scramble game view: renders game in html/svg.

module View exposing (view)

import Html exposing (Html)
import Html.Attributes
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Model exposing (..)
import Vehicle exposing (Vehicle)
import Obstacle exposing (Obstacle)
import List.Extra exposing (getAt)
import Bitwise exposing (xor)
import Update
import Math.Vector2 exposing (..)

coordinate : Float -> String
coordinate n = toString n

roadDepth = 60

box = [ viewBox ("0 0 " ++ (toString totalWidth) ++ " " ++ (toString (totalHeight + roadDepth)))
      , width "1000px" ]
sky = [rect [ width (toString totalWidth)
            , height (toString totalHeight)
            , fillOpacity "1"
            , fill "midnightblue"] []]

border = [rect [ width (toString totalWidth)
               , height (toString (totalHeight + roadDepth))
               , fillOpacity "0"
               , strokeWidth "20"
               , stroke "black"] []]

roadGradient =
  linearGradient [id "RoadGradient"]
    [ stop [offset "0%", stopColor "blue"] []
    , stop [offset "25%", stopColor "black"] []
    , stop [offset "50%", stopColor "blue"] []
    , stop [offset "75%", stopColor "black"] []
    , stop [offset "100%", stopColor "blue"] []]

roadSegment : Int -> Svg msg
roadSegment xPos =
  rect [ width (toString totalWidth)
       , height (toString roadDepth)
       , fillOpacity "1"
       , x (toString xPos)
       , y (toString totalHeight)
       , fill "url(#RoadGradient)"] []

road : Int -> List (Svg msg)
road ticks =
  let offset = -(10 * ticks % totalWidth) in
  [ roadGradient
  , roadSegment offset
  , roadSegment (totalWidth + offset)]

frames : String -> Int -> String -> List String
frames base count suffix =
  List.map (\n -> base ++ (toString n) ++ suffix) (List.range 1 count)

vehicleImages : Vehicle -> List String
vehicleImages vehicle =
  case vehicle of
    Vehicle.Car -> frames "images/car/car-" 2 ".png"
    Vehicle.Drill -> frames "images/drill/drill-" 4 ".png"
    Vehicle.Skateboard -> frames "images/skateboard/skateboard-" 2 ".png"
    Vehicle.Helicopter -> frames "images/helicopter/helicopter-" 4 ".png"
    Vehicle.Pogo -> [ "images/pogo-1.png" ]
    Vehicle.Rocket -> frames "images/rocket/rocket-" 2 ".png"
    Vehicle.Motorcycle -> frames "images/motorcycle/motorcycle-" 2 ".png"

obstacleImages : Obstacle -> List String
obstacleImages obs =
  case obs of
    Obstacle.BounceBall ->
      frames "images/bounceball/bounceball-" 5 ".png"
    Obstacle.BoxingGlove ->
      frames "images/boxingglove/boxingglove-" 12 ".png"
    Obstacle.Card -> frames "images/card/card-" 2 ".png"
    Obstacle.SpikeBall -> [ "images/spikeball-1.png" ]
    Obstacle.SpikeFloor -> [ "images/spikefloor-1.png" ]
    Obstacle.Stalactite -> [ "images/stalactite.png" ]
    Obstacle.SwapBox -> frames "images/swapbox/swapbox-" 4 ".png"

renderVehicle : Model -> Svg msg
renderVehicle model =
  let vehicle = model.vehicle in
  let pos = model.pos in
  let color =
    if model.remainingTransitions == 0 then
      "lightgray"
    else
      "red"
  in
    let pics = vehicleImages vehicle in
    let sharedAtts =
      [ x ((getX pos - Vehicle.backToDriver vehicle)
          |> toString)
      , y ((totalHeight - (getY pos) - Vehicle.topToDriver vehicle)
          |> toString)
      , width (Vehicle.width vehicle |> toString)
      , height (Vehicle.height vehicle |> toString)
      ]
    in
      let index = (model.ticks // 4) % List.length pics in
      case List.Extra.getAt index pics of
        Just pic ->
          image ((xlinkHref pic)::sharedAtts) []
        Nothing ->
          rect (sharedAtts ++
                  [ fillOpacity "1"
                  , fill color
                  , strokeWidth "0.1"
                  , stroke "black"]) []

renderEdges : List Vec2 -> List (Svg msg)
renderEdges vertices =
  if vertices == [] then
    []
  else
    let verticesString =
      List.map (\vertex ->
                    toString (getX vertex) ++ ","
                    ++ ((totalHeight - (getY vertex)) |> toString))
          vertices
      |> String.join " "
    in
    [ polygon [ points verticesString
              , fill "none"
              , strokeWidth "2"
              , stroke "yellow" ] []]

renderObstacle : Int -> Int -> ObstacleState -> Svg msg
renderObstacle ticks remainingTransitions obsState =
  let obs = obsState.obstacle in
  let pos = obsState.pos in
  let pics = obstacleImages obs in
  if List.length pics == 0 then
    rect [ width (toString (Obstacle.width obs))
         , height (toString (Obstacle.height obs))
         , x (toString (getX obsState.pos))
         , y (totalHeight - (getY obsState.pos) |> toString)
         , fillOpacity "1"
         , strokeWidth "1"
         , fill "black"
         , stroke "black"] []
  else
    let index =
      if obs == Obstacle.SwapBox then
        (remainingTransitions % List.length pics)
      else if obs == Obstacle.BounceBall then
        4 - abs (4 - (obsState.extra // 10))
      else if obs == Obstacle.Card then
        obsState.extra
      else
        (ticks // 3) % List.length pics
    in
    let href =
      case List.Extra.getAt index pics of
        Just pic -> pic
        Nothing -> "unreachable"
    in
    let updatedHref =
      if obs == Obstacle.SpikeFloor && (getY obsState.pos) > totalHeight / 2 then
        "images/spikeceiling-1.png"
      else
        href
    in
    let atts =
      [ xlinkHref updatedHref
      , x (toString (getX pos))
      , y ((totalHeight - (getY pos)) |> toString)
      , width (Obstacle.width obs |> toString)
      , height (Obstacle.height obs |> toString)]
    in
    image atts []

renderEgg : Model -> Svg msg
renderEgg model =
  image [ xlinkHref "images/egg-1.png"
        , width "68"
        , height "80"
        , x (coordinate ((getX model.pos) - 34))
        , y (coordinate (totalHeight - (getY model.pos) - 40))] []

wString : String
wString = toString buildingWidth

totalBlocks : Int
totalBlocks = 30

buildingTile : Int -> Vec2 -> Svg msg
buildingTile number pos =
  image [ xlinkHref ("images/dice/dice-" ++ toString number ++ ".png")
        , width wString
        , height wString
        , x (toString (getX pos))
        , y (toString (getY pos)) ] []

buildingDim : Float
buildingDim = toFloat buildingWidth

skyline : Int -> List Building -> List (Svg msg)
skyline ticks buildings =
  List.concatMap
    (\building ->
       let x = toFloat (building.absX - ticks * 2) in
       List.map
       (\floor ->
          let pos = vec2 x floor.y in
          buildingTile floor.value pos) building.floors) buildings

atts : List (Html.Attribute msg)
atts =
  [ Html.Attributes.style
      [ ("padding", "10px 10px")
      , ("text-align", "center")
      ]]

view : Model -> Html Msg
view model =
    let bullets =
        case model.bullet of
          Just bullet ->
            [ circle [ cx (coordinate (getX bullet.pos))
                     , cy (coordinate (totalHeight - (getY bullet.pos)))
                     , r "10"
                     , fill "silver" ] []]
          Nothing -> []
    in
    let eggInVehicle =
      -- Motorcyle has lower z-position than egg, but other
      -- vehicles have a higher z-position than egg.
      if model.vehicle == Vehicle.Motorcycle then
        [ renderVehicle model, renderEgg model ]
      else
        [ renderEgg model, renderVehicle model ]
    in
    let instructions =
      if model.gameOver then
        "Game over! Refresh browser to play again."
      else
        "Controls: 'w', 'a', 'd'"
    in
    let absX = model.ticks * 15 in
    let showEdges = False in
    Html.div
      atts
      [ Html.h2 [] [Html.text "Scramble! by Nathan and Jim Clune"]
      , Html.h3 [] [Html.text (instructions ++ "  Score: " ++ toString model.ticks)]
      , svg box
        (sky
         ++ skyline model.ticks model.buildings
         ++ bullets
         ++ eggInVehicle
         ++ road model.ticks
         ++ List.map (renderObstacle model.ticks model.remainingTransitions) model.obstacles
         ++ (if showEdges then
             List.concatMap (\obsState ->
                                (Update.obstacleVertices obsState model.ticks
                                |> renderEdges)) model.obstacles
             ++ (Update.vehicleVertices model |> renderEdges)
             ++ (case model.bullet of
                   Nothing -> []
                   Just bullet -> Update.bulletVertices bullet |> renderEdges)
             else [])
         ++ border)
      ]
