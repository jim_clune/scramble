-- 2D collision detection using SAT (Separating Axis Theorem)
module Collision exposing (..)
import Math.Vector2 exposing (..)

project : List Vec2 -> Vec2 -> (Float, Float)
project vertices axis =
  case vertices of
    [] -> (0, 0)
    v::vs ->
      let initial = dot v axis in
      List.foldl (\vertex (accMin, accMax) ->
                   let result = dot vertex axis in
                   (min accMin result, max accMax result))
          (initial, initial) vs

overlap : (Float, Float) -> (Float, Float) -> Bool
overlap (min1, max1) (min2, max2) =
  not (max1 <= min2 || max2 <= min1)

edgeDirections : List Vec2 -> List Vec2
edgeDirections vertices =
  case vertices of
    [] -> []
    _::[] -> []
    v1::v2::vs ->
      (sub v1 v2) :: (edgeDirections (v2::vs))

perpendicular : Vec2 -> Vec2
perpendicular v =
  vec2 -(getY v) (getX v)

-- Returns true if the two convex objects are colliding, where the objects are
-- represented by their vertices, listed in the order of a trace around the
-- object perimeter.
convexObjectsCollide : List Vec2 -> List Vec2 -> Bool
convexObjectsCollide vert1 vert2 =
  -- By the Separating Axis Theorem, the convex objects collide if and only if
  -- the projections of the shapes overall for all the relevant axes overlap.
  -- The relevant axes are those that are perpendicular to the edges of the
  -- the shapes.
  let axes = List.map perpendicular ((edgeDirections vert1) ++ (edgeDirections vert2)) in
  List.all
    (\axis ->
       let p1 = project vert1 axis in
       let p2 = project vert2 axis in
       overlap p1 p2) axes
