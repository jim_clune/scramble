# README #

Scramble is a game written in elm for [March/April 2018 Elm Game Jam](https://itch.io/jam/elm-game-jam-mar-apr-2018).
The theme of the game jam was "Randomness".

### Play the game ###
[https://nathanclune.com/scramble.html](https://nathanclune.com/scramble.html)

### Download and build the game ###

* Install [elm](http://elm-lang.org)
* Clone the scramble repo.
* To run on a prebuilt html file:
    * In the scramble directory, run 'elm-make scramble.elm'
    * Point your browser to ./index.html
* To run in elm-reactor:
    * In the scramble directory, run 'elm-reactor'
    * Point your browser to the URL indicated by elm-reactor.
    * Browse to 'scramble.elm'
