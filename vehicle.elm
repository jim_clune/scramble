-- Vehicles that the egg drives throughout the game.

module Vehicle exposing (..)
import Math.Vector2 exposing (..)

type Vehicle
  = Car
  | Drill
  | Helicopter
  | Motorcycle
  | Pogo
  | Rocket
  | Skateboard

ofInt : Int -> Vehicle
ofInt n =
  case n of
    0 -> Car
    1 -> Drill
    2 -> Helicopter
    3 -> Motorcycle
    4 -> Pogo
    5 -> Rocket
    _ -> Skateboard

height : Vehicle -> Float
height vehicle =
  case vehicle of
    Car -> 88
    Drill -> 216
    Helicopter -> 142
    Motorcycle -> 160
    Pogo -> 104
    Rocket -> 128
    Skateboard -> 44

width : Vehicle -> Float
width vehicle =
  case vehicle of
    Car -> 220
    Drill -> 112
    Helicopter -> 216
    Motorcycle -> 196
    Pogo -> 60
    Rocket -> 228
    Skateboard -> 148

-- Vectors are relative to upper left.
vertices : Vehicle -> List Vec2
vertices vehicle =
  case vehicle of
    Car ->
      [ vec2 56 0
      , vec2 148 0
      , vec2 216 -84
      , vec2 24 -84 ]
    Drill ->
      [ vec2 54 0
      , vec2 108 -212
      , vec2 0 -212 ]
    Helicopter ->
      [ vec2 72 -24
      , vec2 164 -28
      , vec2 184 -136
      , vec2 60 -136 ]
    Motorcycle ->
      [ vec2 36 -48
      , vec2 152 -48
      , vec2 176 -156
      , vec2 20 -156 ]
    Pogo ->
      [ vec2 0 0
      , vec2 56 0
      , vec2 28 -100 ]
    Rocket ->
      [ vec2 60 0
      , vec2 224 -62
      , vec2 60 -124 ]
    Skateboard ->
      [ vec2 72 72
      , vec2 132 -40
      , vec2 12 -40 ]

topToDriver : Vehicle -> Float
topToDriver vehicle =
  case vehicle of
    Car -> 8
    Drill -> 144
    Helicopter -> 64
    Motorcycle -> 60
    Pogo -> -28
    Rocket -> 64
    Skateboard -> -24

frontToDriver : Vehicle -> Float
frontToDriver vehicle =
  case vehicle of
    Car -> 110
    Drill -> 58
    Helicopter -> 82
    Motorcycle -> 102
    Pogo -> 30
    Rocket -> 82
    Skateboard -> 76

backToDriver : Vehicle -> Float
backToDriver vehicle =
  width vehicle - frontToDriver vehicle

bottomToDriver : Vehicle -> Float
bottomToDriver vehicle =
  height vehicle - topToDriver vehicle
