import Html
import Time
import Keyboard
import Char
import Model exposing (..)
import Vehicle
-- Scramble! By Nathan and Jim Clune.
-- A game developed as part of the March-April 2018 elmgames.club jam
-- with the theme "Randomness".

import Update exposing (update)
import View exposing (view)
import Math.Vector2 exposing (..)

main =
  Html.program
    { init = init
    , view = View.view
    , update = Update.update
    , subscriptions = subscriptions
    }

init : (Model, Cmd Msg)
init =
  let vehicle = Vehicle.Skateboard in
 ({ pos = vec2 (totalWidth / 2) (Vehicle.bottomToDriver vehicle)
  , vehicle = vehicle
  , lastChanged = 0
  , remainingTransitions = 1
  , left = False
  , right = False
  , mostRecent = Right
  , up = False
  , verticalVelocity = 0
  , ticks = 0
  , obstacles = []
  , bullet = Nothing
  , dropStalactiteAt = totalWidth / 2
  , lastObstacleTime = 0
  , timeBetweenObstacles = 2000
  , buildings = []
  , buildingNumber = 0
  , gameOver = False }, Cmd.none)

subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.batch
      [ Time.every (0.016 * Time.second) Tick
      , Keyboard.downs (\code -> Press (Char.fromCode code))
      , Keyboard.ups (\code -> Release (Char.fromCode code)) ]
