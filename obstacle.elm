-- Various obstacles that egg must navigate.

module Obstacle exposing (..)
import Vehicle exposing (Vehicle)
import Math.Vector2 exposing (..)

type Obstacle
  = BounceBall
  | BoxingGlove
  | Card
  | SpikeBall
  | SpikeFloor
  | Stalactite
  | SwapBox

ofInt : Int -> Obstacle
ofInt n =
  case n of
    0 -> BounceBall
    1 -> BoxingGlove
    2 -> Card
    3 -> SpikeBall
    4 -> SpikeFloor
    5 -> Stalactite
    _ -> SwapBox

height : Obstacle -> Float
height obstacle =
  case obstacle of
    BounceBall -> 120
    BoxingGlove -> 292
    Card -> 1500
    SpikeBall -> 156
    SpikeFloor -> 80
    Stalactite -> 120
    SwapBox -> 160

width : Obstacle -> Float
width obstacle =
  case obstacle of
    BounceBall -> 200
    BoxingGlove -> 92
    Card -> 400
    SpikeBall -> 156
    SpikeFloor -> 1280
    Stalactite -> 120
    SwapBox -> 160

-- Vectors are relative to upper left.
vertices : Obstacle -> List Vec2
vertices obstacle =
  case obstacle of
    BounceBall ->
      [ vec2 40 -60
      , vec2 160 -60
      , vec2 160 -120
      , vec2 40 -120 ]
    BoxingGlove ->
      [ vec2 12 -12
      , vec2 80 -12
      , vec2 80 -68
      , vec2 12 -68 ]
    Card -> [] -- Cards don't collide with anything, so we model it as having no vertices.
    SpikeBall ->
      [ vec2 76 0
      , vec2 0 -76
      , vec2 76 -152
      , vec2 152 -76 ]
    SpikeFloor ->
      [ vec2 0 -18
      , vec2 1276 -18
      , vec2 1276 -60
      , vec2 0 -60]
    Stalactite ->
      [ vec2 0 0
      , vec2 (width obstacle) 0
      , vec2 ((width obstacle) / 2) -(height obstacle)
      , vec2 0 0 ]
    --SwapBox -> []
    _ ->
      [ vec2 0 0
      , vec2 (width obstacle) 0
      , vec2 (width obstacle) -(height obstacle)
      , vec2 0 -(height obstacle)
      , vec2 0 0 ]

-- A measure of how difficult the obstacle is to manuever.
-- Intended for use in limiting the total difficulty of obstacles on screen.
difficulty : Obstacle -> Int
difficulty obstacle =
  case obstacle of
    BounceBall -> 2
    BoxingGlove -> 3
    Card -> 0
    SpikeBall -> 1
    SpikeFloor -> 4
    Stalactite -> 1
    SwapBox -> 1

causesGameOver : Obstacle -> Bool
causesGameOver obstacle =
  case obstacle of
    BounceBall -> True
    BoxingGlove -> True
    Card -> False
    SpikeBall -> True
    SpikeFloor -> True
    Stalactite -> True
    SwapBox -> False

-- Not all vehicles can navigate all objects. Pick from compatible objects
-- to give the egg a fighting chance.
compatibleWith : Vehicle -> List Obstacle
compatibleWith vehicle =
  case vehicle of
    Vehicle.Car ->
      [ BounceBall
      , Card
      , Stalactite
      , SwapBox ]
    Vehicle.Drill ->
      [ BounceBall
      , BoxingGlove
      , Card
      , SpikeBall
      , SpikeFloor
      , Stalactite
      , SwapBox ]
    Vehicle.Helicopter ->
      [ BounceBall
      , BoxingGlove
      , Card
      , SpikeBall
      , SpikeFloor
      , Stalactite
      , SwapBox ]
    Vehicle.Motorcycle ->
      [ BounceBall
      , Card
      , Stalactite
      , SwapBox ]
    Vehicle.Pogo ->
      [ BounceBall
      , BoxingGlove
      , Card
      , SpikeBall
      , Stalactite
      , SwapBox ]
    Vehicle.Rocket ->
      [ BounceBall
      , BoxingGlove
      , Card
      , SpikeBall
      , SpikeFloor
      , Stalactite
      , SwapBox ]
    Vehicle.Skateboard ->
      [ BounceBall
      , BoxingGlove
      , Card
      , SpikeBall
      , Stalactite
      , SwapBox ]
