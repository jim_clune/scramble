-- Game model and some additional types and constants used
-- throughout the game.
module Model exposing (..)
import Time exposing (Time)
import Vehicle exposing (Vehicle)
import Obstacle exposing (Obstacle)
import Math.Vector2 exposing (..)

type alias Bullet =
  { pos : Vec2
    -- Positive velocity is upward.
  , verticalVelocity : Float }

type alias ObstacleState =
  { obstacle : Obstacle
  , pos : Vec2
  , verticalVelocity : Float
    -- Extra is an integer that can optionally be used to track additional
    -- obstacle-specific state information.
  , extra : Int }

type Horizontal = Left | Right

type Msg
  = Tick Time
  | Press Char
  | Release Char
  | SetVehicle Int
  | SpawnObstacle (Int, Float)
  | SpawnBuilding (List Int)

totalWidth = 1500
totalHeight = 750
gravity = -1

type alias Floor =
  { value : Int
  , y : Float }

type alias Building =
  { floors : List Floor
  , absX : Int }

buildingWidth : Int
buildingWidth = 60

generateBuilding : Int -> List Int -> Building
generateBuilding number floorsAndValues =
  let values =
    case floorsAndValues of
      [] -> []
      floors::vals ->
        List.take floors vals
  in
  let floors =
    List.indexedMap (\index value ->
                        ({ value = value
                         , y = totalHeight - toFloat (buildingWidth * (index + 1)) })) values
  in
  { floors = floors, absX = number * buildingWidth }

type alias Model =
  { pos : Vec2
  , vehicle : Vehicle
    -- time we last changed vehicles
  , lastChanged : Time
    -- the number of remaining vehicle transitions we need to make for this
    -- transition cycle, or 0 if we're not transitioning
  , remainingTransitions : Int
    -- true if left button is pressed
  , left : Bool
    -- true if right button is pressed
  , right : Bool
    -- which horizontal direction has most recently been pressed
  , mostRecent : Horizontal
    -- true if up button is pressed
  , up : Bool
    -- Positive velocity is upward.
  , verticalVelocity : Float
  , ticks : Int
  , obstacles : List ObstacleState
  , bullet : Maybe Bullet
  , dropStalactiteAt : Float
  , lastObstacleTime : Time
  , timeBetweenObstacles : Float
  , buildings : List Building
  , buildingNumber : Int
  , gameOver : Bool }
